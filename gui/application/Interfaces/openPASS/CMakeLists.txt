# /*********************************************************************
# * Copyright (c) 2019 Volkswagen Group of America.
# *
# * This program and the accompanying materials are made
# * available under the terms of the Eclipse Public License 2.0
# * which is available at https://www.eclipse.org/legal/epl-2.0/
# *
# * SPDX-License-Identifier: EPL-2.0
# **********************************************************************/

set(HEADERS
    ${HEADERS}
    ${CMAKE_CURRENT_LIST_DIR}/PluginInterface.h
    ${CMAKE_CURRENT_LIST_DIR}/PluginManagerInterface.h
    ${CMAKE_CURRENT_LIST_DIR}/RandomInterface.h
    ${CMAKE_CURRENT_LIST_DIR}/ServiceInterface.h
    ${CMAKE_CURRENT_LIST_DIR}/ServiceManagerInterface.h
    ${CMAKE_CURRENT_LIST_DIR}/IOPluginInterface.h)
